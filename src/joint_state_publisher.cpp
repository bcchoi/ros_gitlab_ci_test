#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <string>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <actionlib/server/simple_action_server.h>
#pragma GCC diagnostic pop
#include <box_robot_definition/MoveBoxAction.h>

std::shared_ptr<ros::Publisher> joint_pub;
std::shared_ptr<sensor_msgs::JointState> joint_states;

class MoveBoxAction
{
protected:
  ros::NodeHandle nh_;
  // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  actionlib::SimpleActionServer<box_robot_definition::MoveBoxAction> as_;
  // Create messages that are used to published feedback/result
  box_robot_definition::MoveBoxFeedback feedback_;
  box_robot_definition::MoveBoxResult result_;

public:
  MoveBoxAction(std::string name) :
    as_(nh_, name, boost::bind(&MoveBoxAction::executeCB, this, _1), false)
  {
    as_.start();
  }

  void executeCB(const box_robot_definition::MoveBoxGoalConstPtr &goal)
  {
    feedback_.percent_complete = 0;
    as_.publishFeedback(feedback_);
    ros::Duration(0.2).sleep();

    joint_states->position[0] = goal->z_angle;

    feedback_.percent_complete = 50;
    as_.publishFeedback(feedback_);
    ros::Duration(0.2).sleep();

    feedback_.percent_complete = 100;
    as_.publishFeedback(feedback_);
    ros::Duration(0.2).sleep();

    result_.success = true;
    as_.setSucceeded(result_);
  }
};

void jointStatePublishCallback(const ros::TimerEvent&)
{
  joint_states->header.stamp = ros::Time::now();
  joint_pub->publish(*joint_states);
}

int main(int argc,
         char** argv)
{
  ros::init(argc, argv, "joint_state_publisher");
  ros::NodeHandle nh;

  // Get parameters
  int publish_rate;
  nh.param("/joint_state_publisher/rate", publish_rate, 60); // Hz

  // Create JointState message
  joint_states.reset(new sensor_msgs::JointState);
  joint_states->position.push_back(0);
  joint_states->name.push_back("box");

  // JointState message publisher
  joint_pub.reset(new ros::Publisher);
  *joint_pub = nh.advertise<sensor_msgs::JointState>("joint_states", 1);

  // Start sending JointState messages (forever)
  ros::Timer timer = nh.createTimer(ros::Duration(1.0 / publish_rate), jointStatePublishCallback);

  // Create actionlib server
  MoveBoxAction move_box("move_box");

  ros::MultiThreadedSpinner spinner(2);
  spinner.spin(); // spin() will not return until the node has been shutdown
  return 0;
}
